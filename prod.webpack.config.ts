import config from './dev.webpack.config';

config.mode = 'production';

export default config;