import * as React from 'react';
import { render } from 'react-dom';

import { Navbar } from './navbar/navbar';

render(
    <Navbar />,
    document.getElementById('app')
);