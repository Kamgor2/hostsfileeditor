import * as nw from 'nw.gui';

const windowOptions = {};

const openWindow = () => {
    return new Promise((resolve) => {
        nw.Window.open(
            require('./index.html'),
            windowOptions,
            (new_win) => {
                resolve(new_win);
            }
        );
    })
}

openWindow()
    .then((win)=>{
        console.log(win);
    })